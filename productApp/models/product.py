from django.db import models
from .category import Category

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Name', max_length=30)
    price = models.PositiveIntegerField(default=0)
    photo = models.CharField('url image', max_length=30)
    characteristics = models.TextField('characteristics')
    category = models.ForeignKey(Category, related_name='product', on_delete=models.CASCADE)
    