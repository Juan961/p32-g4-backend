from rest_framework import serializers
from productApp.models.category import Category

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'name']

    def create(self, validated_data):
        categoryInstance = Category.objects.create(**validated_data)
        return categoryInstance

    def to_representation(self, obj):
        category = Category.objects.get(id=obj.id)

        return {
            'id': category.id,
            'name': category.name
        }
