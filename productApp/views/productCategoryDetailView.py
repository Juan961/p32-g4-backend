from rest_framework import status, views
from rest_framework.response import Response
from productApp.serializers.productSerializer import ProductSerializer
from productApp.models import Product

class ProductCategoryDetailView(views.APIView):

    def get(self, request, **kwargs):

        products = Product.objects.filter(category=kwargs['pk'])
        
        serializer = ProductSerializer(products, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
