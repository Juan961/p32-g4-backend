from rest_framework import views
from rest_framework.response import Response
from productApp.serializers.productSerializer import ProductSerializer
from productApp.models import Product

class ProductDetailView(views.APIView):

    def get(self, request, **kwargs):

        product = Product.objects.get(id=kwargs['pk'])
        
        serializer = ProductSerializer(product)

        return Response(serializer.data)
