from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from productApp.serializers.productSerializer import ProductSerializer
from productApp.models import Product

class ProductCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)
    

    # Return a list of all products

    def get(self, request, *args, **kwargs):
        
        category = request.GET.get('category')
        name = request.GET.get('name')

        if name:
            name = name.rsplit(name[-1])[0]
            products = Product.objects.filter(name=str(name))
        elif category:
            category = category.rsplit(category[-1])[0]
            products = Product.objects.filter(category=str(category))
        else:
            products = Product.objects.all()

        serializer = ProductSerializer(products, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
